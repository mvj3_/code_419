1、NavigationController切换UIViewController的两种方式
方法一右侧进入
SecondViewController* svc=[[SecondViewController alloc]init];
[self.navigationController pushViewController:fvc animated:YES];
返回到上一个
[self.navigationController popViewControllerAnimated:YES];
另一种方法从下面切入
SecondViewController* svc=[[SecondViewController alloc]init];
[self.navigationController presentModalViewController:svc animated:YES];
[svc release];
返回到上一个UIViewController
[self.navigationController dismissModalViewControllerAnimated:YES];
 2、如果没有导航栏NavigationController的话 也是可以切换的
SecondViewController* svc=[[SecondViewController alloc]init];
[self presentModalViewController:svc animated:YES];
[svc release];
 返回到上一个UIViewController
[self dismissModalViewControllerAnimated:YES];